import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserModel } from '../models/UserModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {  }

  public login(body: UserModel): Observable<any> {
    const json = JSON.stringify(body);
    const url = environment.api_url + '/api/token';
    return this.http.post(url, json);
  }

  public register(body: UserModel): Observable<any> {
    const json = JSON.stringify(body);
    const url = environment.api_url + '/api/registerUser';
    return this.http.post(url, json);
  }

}
