import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from '../login-routing.module';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from '../register/register.component';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginService } from '../login.service';
import { of, observable, throwError } from 'rxjs';
import { AuthService } from 'src/app/shared/àuth/auth.service';
import { Router } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { error } from 'util';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const loginServiceSpy = jasmine.createSpyObj('loginService', ['login']);
  const authServiceSpy = jasmine.createSpyObj('authServiceSpy', ['saveToken', 'removeToken']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, RegisterComponent],
      imports: [
        CommonModule,
        LoginRoutingModule,
        MatInputModule,
        MatCardModule,
        ReactiveFormsModule,
        MatButtonModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(
          []
        ),
        ToastrModule.forRoot()
      ], providers: [
        { provide: LoginService, useValue: loginServiceSpy },
        { provide: AuthService, useValue: authServiceSpy }
      ]
    }).compileComponents();

    loginServiceSpy.login.and.returnValue(of('somevalue'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  it('should create loginComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should call authService to save token in storage on submitting form succesfully', () => {
    component.onSubmit();

    expect(loginServiceSpy.login).toHaveBeenCalled();
    expect(authServiceSpy.saveToken).toHaveBeenCalled();
  });

  it('should show an error on an insuccesfull login', () => {
    loginServiceSpy.login.and.returnValue(throwError('error'));

    component.onSubmit();

    expect(component.errorMessage).toBeDefined();
  });

});
