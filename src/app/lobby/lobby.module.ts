import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LobbyComponent} from './lobby.component';
import {MatCardModule} from '@angular/material';
import {HeaderModule} from '../header/header.module';
import {LobbyRoutingModule} from './lobby-routing.module';
import { LobbyService } from './lobby.service';
import { ChatModule } from '../chat/chat.module';


@NgModule({
  declarations: [LobbyComponent],
  exports: [
    LobbyComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    HeaderModule,
    LobbyRoutingModule,
    ChatModule,
  ],
  providers: [LobbyService],
})
export class LobbyModule {
}
