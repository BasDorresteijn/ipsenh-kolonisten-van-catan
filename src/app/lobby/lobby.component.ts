import {Component, OnInit} from '@angular/core';
import {UserModel} from '../models/UserModel';
import {FriendsService} from '../friends/friends.service';
import {LobbyService} from './lobby.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  public usersInLobby = [];

  constructor(private friendService: FriendsService, private lobbyService: LobbyService, private router: Router) {
  }

  ngOnInit() {
    this.fillUsersInLobbyList();
    this.setBackgroundColor();
  }

  setBackgroundColor() {
    document.body.style.backgroundColor = '#D32E00';
  }

  fillUsersInLobbyList() {
    this.lobbyService.getUsersInLobby().subscribe(
      val => {
        this.usersInLobby = val;
        console.log(this.usersInLobby);
      }
    );
  }

  startGame() {
    this.lobbyService.startGame();
    this.router.navigate(['game']);
  }

}
