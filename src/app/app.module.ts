import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginModule} from './login/login.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {httpInterceptorProviders} from 'src/http-interceptors';
import {HomeModule} from './home/home.module';
import {FriendsModule} from './friends/friends.module';
import {ScoreboardModule} from './scoreboard/scoreboard.module';
import {ToastrModule} from 'ngx-toastr';
import {GameboardComponent} from './gameboard/gameboard.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {DiceComponent} from './gameboard/dice/dice.component';
import {LobbyModule} from './lobby/lobby.module';
import {HeaderModule} from './header/header.module';
import {GameboardModule} from './gameboard/gameboard.module';
import { ChatModule } from './chat/chat.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HomeModule,
    FriendsModule,
    ScoreboardModule,
    ToastrModule.forRoot(),
    LobbyModule,
    HeaderModule,
    GameboardModule,
    ChatModule
  ],
  providers: [httpInterceptorProviders, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
