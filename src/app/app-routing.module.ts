import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '', loadChildren: () => import('./login/login.module').then(m => m.LoginModule), /* canActivateChild: [AuthGuard] */},
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), canActivateChild: [AuthGuard]},
  {path: '', loadChildren: () => import('./friends/friends.module').then(m => m.FriendsModule), canActivateChild: [AuthGuard]},
  {path: '', loadChildren: () => import('./lobby/lobby.module').then(m => m.LobbyModule), canActivateChild: [AuthGuard]},
  {
    path: '',
    loadChildren: () => import('./scoreboard/scoreboard.module').then(m => m.ScoreboardModule), canActivateChild: [AuthGuard]
  },
  {path: '', loadChildren: () => import('./gameboard/gameboard.module').then(m => m.GameboardModule), canActivateChild: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
