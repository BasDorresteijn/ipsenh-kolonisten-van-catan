import { TestBed } from '@angular/core/testing';

import { GameboardService } from './gameboard.service';
import { HttpClientModule } from '@angular/common/http';

describe('GameboardService', () => {

  let gameboardService: GameboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    gameboardService = TestBed.get(GameboardService);
  });

  it('should be created', () => {
    const service: GameboardService = TestBed.get(GameboardService);
    expect(service).toBeTruthy();
  });
});
