import { Injectable } from '@angular/core';
import { EventEmitter } from 'events';
import { DiceModel } from 'src/app/models/DiceModel';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DiceService {

  constructor(private http: HttpClient) { }

  private result: EventEmitter = new EventEmitter();
  private dice: EventEmitter = new EventEmitter();

  getDiceEventEmitter(): EventEmitter {
    return this.dice;
  }

  getDiceResultEventEmitter(): EventEmitter {
    return this.result;
  }

  rollDice() {
    this.dice.emit('roll');
  }

  emitDiceResult(result: DiceModel) {
    this.result.emit('', result);
  }

  requestDiceRollApi() {
    const url = environment.api_url + '/api/diceResult';
    return this.http.get(url);
  }

}
