import { TestBed } from '@angular/core/testing';

import { DiceService } from './dice.service';
import { HttpClientModule } from '@angular/common/http';

describe('DiceService', () => {

  let diceService: DiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    diceService = TestBed.get(DiceService);
  });

  it('should be created', () => {
    expect(diceService).toBeTruthy();
  });

  it('should have a method for requesting a diceRoll from the api', () => {
    expect(diceService.requestDiceRollApi).toBeTruthy();
  });

  it('should create two events', () => {
    expect(diceService.getDiceEventEmitter).toBeTruthy();
    expect(diceService.getDiceResultEventEmitter).toBeTruthy();
  });

});
