import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameboardComponent} from './gameboard.component';
import {CommonModule} from '@angular/common';
import {GameboardRoutingModule} from './gameboard-routing.module';
import {HeaderModule} from '../header/header.module';
import {RandomResourceName} from '../models/ResourceNames';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {DiceComponent} from './dice/dice.component';
import {GameboardimageComponent} from './gameboardimage/gameboardimage.component';
import {VillageTileModel} from '../models/VillageTileModel';
import {BuildTileModel} from '../models/BuildTileModel';

describe('GameboardComponent', () => {
  let component: GameboardComponent;
  let fixture: ComponentFixture<GameboardComponent>;
  const gameBoard = new GameboardComponent(null, null, null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GameboardComponent, DiceComponent, GameboardimageComponent],
      imports: [
        CommonModule,
        GameboardRoutingModule,
        HeaderModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(
          []
        )],
      providers: [RandomResourceName]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(GameboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should return leftVillage', () => {
    expect(component.generateLeftVillage()).toEqual(jasmine.any(VillageTileModel));
  });

  it('Should return rightVillage', () => {
    expect(component.generateRightVillage()).toEqual(jasmine.any(VillageTileModel));
  });

  it('GameBoard must be defined', () => {
    component.createBaseGameboard();
    expect(component.gameBoard).toBeDefined();
  });


  it('backGroundColor White', () => {
    component.setBackgroundColor();
    expect(document.body.style.backgroundColor).toEqual('white');
  });

  it('isSelected', () => {
    component.isSelected(new BuildTileModel());
    component.eventEmitter.on('', (val) => {
      expect(val).toBeDefined();
    });
  });
});
