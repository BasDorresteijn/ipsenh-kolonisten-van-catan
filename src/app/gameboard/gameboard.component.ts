import {Component, OnInit, Output, ViewChild} from '@angular/core';
import {GameBoardModel} from '../models/GameBoardModel';
import {StreetTileModel} from '../models/StreetTileModel';
import {VillageTileModel} from '../models/VillageTileModel';
import {ResourceTileModel} from '../models/ResourceTileModel';
import {ResourceNames} from '../models/ResourceNames';
import {DiceService} from './dice/dice.service';
import {DiceModel} from '../models/DiceModel';
import {BuildTileModel} from '../models/BuildTileModel';
import {TileModel} from '../models/TileModel';
import {GameboardimageComponent} from './gameboardimage/gameboardimage.component';
import {EventEmitter} from 'events';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {DiceAndBoardModel} from '../models/DiceAndBoardModel';
import {GameboardService} from './gameboard.service';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
export class GameboardComponent implements OnInit {
  @ViewChild(GameboardimageComponent, {static: false}) gameBoardImageComponent: GameboardimageComponent;

  public gameBoard: GameBoardModel;
  public handClassName = 'handCard';
  public hand: TileModel[] = [
    new BuildTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
    new BuildTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
    new BuildTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
    new StreetTileModel(),
    new VillageTileModel(),
  ];
  eventEmitter = new EventEmitter();

  constructor(private diceService: DiceService, private http: HttpClient, private gameboardService: GameboardService) {
  }

  ngOnInit() {
    this.setBackgroundColor();
    this.subscribeToDiceResultEvent();
    this.eventEmitter.on('placed', (val: TileModel) => {
      this.isSelected(null);
      this.hand.splice(this.hand.indexOf(val), 1);
    });
    this.createBaseGameboard();
  }

  private subscribeToDiceResultEvent() {
    this.diceService.getDiceResultEventEmitter().on('', (diceModel: DiceModel) => {
      this.updateGameboardWithDiceResult(diceModel);
    });
  }

  public createBaseGameboard() {
    this.gameBoard = new GameBoardModel();
    this.gameBoard.centerStreet = new StreetTileModel();
    this.gameBoard.centerStreet.left = this.generateLeftVillage();
    this.gameBoard.centerStreet.right = this.generateRightVillage();
  }

  public generateLeftVillage(): VillageTileModel {
    const leftVillage = new VillageTileModel();
    leftVillage.topleft = new ResourceTileModel(ResourceNames.GRAIN, 1, 2);
    leftVillage.topright = new ResourceTileModel(ResourceNames.GOLD, 1, 1);
    leftVillage.bottomleft = new ResourceTileModel(ResourceNames.SHEEP, 1, 4);
    leftVillage.bottomright = new ResourceTileModel(ResourceNames.WOOD, 1, 5);
    leftVillage.left = null;
    leftVillage.right = null;
    return leftVillage;
  }

  public generateRightVillage(): VillageTileModel {
    const rightVillage = new VillageTileModel();
    rightVillage.topleft = null;
    rightVillage.topright = new ResourceTileModel(ResourceNames.ORE, 1, 3);
    rightVillage.bottomleft = null;
    rightVillage.bottomright = new ResourceTileModel(ResourceNames.STONE, 1, 6);
    rightVillage.left = null;
    rightVillage.right = null;
    return rightVillage;
  }

  public updateGameboardWithDiceResult(diceModel: DiceModel) {
    this.isSelected(null);
    this.gameboardService.updateBoardTiles(this.gameBoard, diceModel).subscribe(
      (val: DiceAndBoardModel) => {
        this.gameBoard = val.gameBoard;
      }, err => {
        this.gameboardService.procesError(err);
      }
    );
  }

  isSelected(tile: TileModel) {
    this.eventEmitter.emit('', tile);
  }

  setBackgroundColor() {
    document.body.style.backgroundColor = 'White';
  }

}



