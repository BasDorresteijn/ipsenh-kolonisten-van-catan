import { CardTypes } from './CardTypes';

export class TileModel {
  constructor(
    public resourceImagePath: string,
    public type: CardTypes
  ) {
  }
}
