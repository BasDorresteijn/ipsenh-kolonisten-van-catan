import {TileModel} from './TileModel';
import {ResourceTileModel} from './ResourceTileModel';
import {StreetTileModel} from './StreetTileModel';
import {CardTypes} from './CardTypes';
import {BuildTileModel} from './BuildTileModel';

export class VillageTileModel extends TileModel {

  topleft: ResourceTileModel;
  topright: ResourceTileModel;
  bottomleft: ResourceTileModel;
  bottomright: ResourceTileModel;
  top: BuildTileModel;
  bottom: BuildTileModel;
  left: StreetTileModel;
  right: StreetTileModel;

  constructor() {
    super('/assets/images/dorp.png', CardTypes.VILLAGE);
  }
}
