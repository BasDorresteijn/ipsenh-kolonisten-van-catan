import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { TokenModel } from 'src/app/models/TokenModel';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthService', () => {

  let testToken: TokenModel;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ]
    });
    authService = TestBed.get(AuthService);
  });

  beforeEach(() => {
    testToken = new TokenModel();
    // tslint:disable-next-line: max-line-length
    testToken.access = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTc1NjI2OTIzLCJqdGkiOiI0NWY2MjM3MzY1ZjY0YWEwODhjODE4YmNjYzUxZGRkNyIsInVzZXJfaWQiOjEwfQ.ehjN7TrU7na7qwa_HkbWxlNXjP9ufqYtm_eLNzX-IUQ';
    // tslint:disable-next-line: max-line-length
    testToken.refresh = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU3NTcxMzAyMywianRpIjoiOWE0MTMzODMwNWMxNDViZTllM2FkMGYyMjMzMWMyZjYiLCJ1c2VyX2lkIjoxMH0.tFu4tJSU9nqM8jp_DF_lSnxNt4ES1v0Qpiprr25RfsA';
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should be able to store token in localStorage', () => {
    const expectedValue = testToken.access;

    authService.saveToken(testToken);
    const actualValue = authService.getAccessToken();

    expect(actualValue).toBe(expectedValue);
  });

  it('isLogged should return True if user has a token in localStorage', () => {
    const expectedValue = true;

    authService.saveToken(testToken);
    const actualValue = authService.isLoggedIn();

    expect(actualValue).toBe(expectedValue);
  });

  it('token should be expired', () => {
    const expectedValue = true;

    authService.saveToken(testToken);
    const actualValue = authService.isTokenExpired();

    expect(actualValue).toBe(expectedValue);
  });

  it('should have a method for requesting a new token from the api', () => {
    expect(authService.requestNewToken()).toBeTruthy();
  });

});
