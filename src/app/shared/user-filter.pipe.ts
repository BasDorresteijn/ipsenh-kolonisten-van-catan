import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

  transform(item: any, searchTerm: string): any {
    if (!item || !searchTerm) {
      return item;
    }
    return item.filter(user => {
      const username = user.username.toLowerCase().includes(searchTerm.toLowerCase());
      return (username);

    });
  }

}
