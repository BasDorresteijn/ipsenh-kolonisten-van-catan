import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFilterPipe } from './user-filter.pipe';


@NgModule({
  declarations: [UserFilterPipe],
  exports: [
    UserFilterPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
