import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FriendsService} from '../friends.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.css']
})
export class AddFriendComponent implements OnInit {
  form: any;
  addFriendInput: string;

  constructor(private dialogRef: MatDialogRef<AddFriendComponent>, private friendService: FriendsService, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  addFriend(usernameFriend) {
    this.friendService.addFriend(usernameFriend).subscribe(
      val => {
        this.toastr.success(usernameFriend + ' toegevoegd!');
        this.friendService.result.emit('');
        this.closeDialog();

      },
      err => {
        console.log('something went wrong', err);
        this.toastr.error('Gebruiker bestaat niet!');
      }
    );
  }
}
