import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LobbyService } from '../lobby/lobby.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  chatLog = '';
  chatSocket: WebSocket;

  chatForm = this.formBuilder.group({
    message: ['', Validators.required],
  });

  constructor(private formBuilder: FormBuilder, private lobbyService: LobbyService) {   }

  ngOnInit() {
    this.lobbyService.getLobbyForUser().subscribe(
      response => {
          const lobbyId = response.id;
          this.createWebSocket(lobbyId);
          this.updateChatBox();
      }
    );
  }

  createWebSocket(lobbyId) {
      this.chatSocket = new WebSocket(
          'ws://' + '145.97.16.193:8000' +
          '/ws/' + lobbyId + '/');
  }

  updateChatBox() {
    this.chatSocket.onmessage = (e) => {
      const data = JSON.parse(e.data);
      const message = data.message;
      this.chatLog = this.chatLog + message + '\n';
    };
  }
  sendMessage() {
    this.chatSocket.send(JSON.stringify({
      message: this.chatForm.controls.message.value
    }));
    this.chatForm.reset();
  }
}

