import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScoreboardComponent} from './scoreboard.component';
import {HeaderModule} from '../header/header.module';
import {MatCardModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {ScoreboardRoutingModule} from './scoreboard-routing.module';
import {ScoreboardService} from './scoreboard.service';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ScoreboardComponent],
  exports: [
    ScoreboardComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    MatCardModule,
    NgxPaginationModule,
    ScoreboardRoutingModule,
    SharedModule,
    FormsModule
  ],
  providers: [ScoreboardService]
})
export class ScoreboardModule {
}
