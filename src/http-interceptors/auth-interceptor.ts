import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/àuth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) { }

    skipInterceptorOneTime = false;

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        if (this.skipInterceptorOneTime) {
            this.skipInterceptorOneTime = false;
            return next.handle(req);
        }
        if (!this.authService.isLoggedIn()) {
            return next.handle(req);
        }
        if (this.authService.isTokenExpired()) {
            this.skipNextRequest();
            return this.requestNewTokenAndSendOriginalRequest(req, next);
        } else {
            return next.handle(this.updateRequestWithAuthorization(req));
        }
    }

    private skipNextRequest(): void {
        this.skipInterceptorOneTime = true;
    }

    updateRequestWithAuthorization(req: HttpRequest<any>): HttpRequest<any> {
        const authReq = req.clone({
            headers: req.headers.append('Authorization', 'Bearer ' + this.authService.getAccessToken())
        });
        return authReq;
    }

    requestNewTokenAndSendOriginalRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return this.authService.requestNewToken().pipe(
            mergeMap(val => {
                this.authService.saveAccessTokenAndExpiredate(val.access);
                return next.handle(this.updateRequestWithAuthorization(req));
            })
        );
    }
}
